import mimetypes
from flask import Flask, render_template, request, url_for, flash, redirect, send_from_directory
from flask_restful import Api
from pysondb import db
import os

app = Flask(__name__)
api = Api(app)
app.config['SECRET_KEY'] = 'e7d4f16b9f2f8de3c993d896de9b152f8e71e9d1dd7cadea'
# We need to supply trainees as JSON
database = db.getDb("backend/database/db.json")
test_report_db = db.getDb("backend/database/test-reports.json")

cohorts_file = 'backend/res/cohorts.txt'


def getTraineeProgress(trainee_id):
    completed = 0
    test_reports = test_report_db.getByQuery({"trainee_id": trainee_id})
    if len(test_reports)==0 :
        return completed
    latest_report = test_reports.pop()
    test_records = latest_report["test_records"]
    for record in test_records:
        if record['complete']:
            completed+=1
    return completed


def getAllTraineeProgress():
    trainees=database.getAll()
    progress_dic = {}
    for trainee in trainees:
        id = trainee['id']
        progress_dic[id] = getTraineeProgress(id)
    return progress_dic  


def getTraineeExercises(trainee_id):
    exercises = []
    test_reports = test_report_db.getByQuery({"trainee_id": int(trainee_id)})
    if len(test_reports)==0 :
        return exercises
    latest_report = test_reports.pop()
    test_records = latest_report["test_records"]
    for record in test_records:
        exercises.append(record["directory"])
    return sorted(exercises)

def getAllTraineeExercises():
    trainees=database.getAll()
    progress_dic = {}
    for trainee in trainees:
        id = trainee['id']
        progress_dic[id] = getTraineeExercises(id)
    return progress_dic  


def getExerciseTestSummary(trainee_id):
    test_reports = test_report_db.getByQuery({"trainee_id": int(trainee_id)})
    test_progress = {}
    if len(test_reports)==0 :
        return {}
    latest_report = test_reports.pop()
    test_records = latest_report["test_records"]
    for record in test_records:
        directory = record["directory"]
        total = record["results"]["summary"]["total"]
        passed = 0
        if 'passed' in record["results"]["summary"]:
            passed = record["results"]["summary"]["passed"]
        test_progress[directory] = f"{passed}/{total}"
    return test_progress

def getCohorts():
    with open(cohorts_file) as file:
        cohorts = [line.rstrip() for line in file]
    return cohorts


def addCohort(cohort):
    cohorts = getCohorts()
    cohorts.append(cohort)
    with open(cohorts_file, 'w') as f:
        for item in cohorts:
            f.write("%s\n" % item)


# Renders HTML pages
@app.route('/')
def index():
    return render_template(
        'trainees.html',
        trainees = database.getAll(),
        progress = getAllTraineeProgress(),
        exercises = getAllTraineeExercises())


@app.route('/create/', methods=('GET', 'POST'))
def create():
    if request.method == 'POST':
        if request.form['action'] == 'Submit':
            first_name = request.form['first_name']
            last_name = request.form['last_name']
            cohort = request.form['cohort']
            bitbucket = request.form['bitbucket']

            if not first_name:
                flash('First name is required!')
            elif not last_name:
                flash('Last name is required!')
            elif not cohort:
                flash('Cohort is required!')
            else:
                database.add({
                    "first_name": first_name,
                    "last_name": last_name,
                    "cohort": cohort,
                    "bitbucket_url": bitbucket})
                return redirect(url_for('index'))
        elif request.form['action'] == 'Add':
            new_cohort = request.form['new_cohort']
            addCohort(new_cohort)

    return render_template(
        'create.html',
        cohorts = getCohorts())


@app.route('/update', methods=('GET', 'POST'))
def update():
    trainee_id = request.args.get('trainee_id', None)
    if request.method == 'POST':
        first_name = request.form.get('first_name')
        last_name = request.form.get('last_name')
        cohort = request.form.get('cohort')
        bitbucket = request.form.get('bitbucket')

        if not first_name:
            flash('First name is required!')
        elif not last_name:
            flash('Last name is required!')
        elif not cohort:
            flash('Cohort is required!')
        else:
            database.updateById(trainee_id, {
                "first_name": first_name,
                "last_name": last_name,
                "cohort": cohort,
                "bitbucket_url": bitbucket})
            return redirect(url_for('index'))
    return render_template(
        'update.html',
        trainee = database.getById(trainee_id),
        cohorts = getCohorts())
        

@app.route('/reports', methods=('GET', 'POST'))
def reports():
    trainee_id = request.args.get('trainee_id', None)
    return render_template('report-list.html',
    trainee=database.getById(trainee_id),
    exercises=getTraineeExercises(trainee_id),
    test_summary=getExerciseTestSummary(trainee_id))


@app.route('/exercise_report', methods=('GET', 'POST'))
def exercise_report():
    trainee_id = request.args.get('trainee_id', None)
    trainee = database.getById(trainee_id)
    exercise_name = request.args.get('exercise_name', None)
    bitbucket_link = trainee['bitbucket_url']+"exercises/"+exercise_name
    return render_template(
        'exercise_report.html',
        trainee_id=trainee_id,
        exercise_name=exercise_name,
        bitbucket_link = bitbucket_link)


@app.route('/favicon.ico')
def favicon(): 
    pwd = os.path.abspath(os.getcwd())
    return send_from_directory(
        pwd,
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon')


@app.route('/assets/style.css')
def stylesheet(): 
    pwd = os.path.abspath(os.getcwd())
    return send_from_directory(
        pwd+'/backend/templates/html_reports/assets',
        'style.css')


if __name__ == '__main__':

    print("This is the MAIN!")
    print(__name__)
    app.run("0.0.0.0", port=80, debug=True)

    
