# Final Project - Trainer Repo
## Executing build
To build the project follow the following instructions
```bash
# 1. make sure you are in the root of this repo
$ cd /path/to/repo
# 2. run the command 
$ ./start_webpage.sh
# 3. visit http://localhost/ to see the list of trainees
```

## Adding new Trainees
1. visit http://localhost/
2. click on the create tab at the top of the page
3. fill in the form and click submit
4. you should be redirected to the overview page with your new trainee added

## Editing Existing Trainees
1. visit http://localhost/ to see the list of trainees
2. click the edit link for the trianee you want to change details for
3. fill in the form and click submit
4. you should be redirected to the overview page with your trainee's data updated

## Adding a new Cohort
1. follow steps 1-2 of Adding a new Trainee
2. enter the name of your ne cohort in the text box labeled `New Cohort`
3. press the add button, you will see your new cohort as an option for the `Cohort` field

## Getting Test results
to get test result for trainees, you will need to add the link to their bitbucket repo. You will need to ask your trainees to send this to you once they have forked the exercise repo. This link will just be the url address of the page when they first open their project on bitbucket e.g. : `https://bitbucket.org/<username>/<project>/src/master/`. For testing to work, this repo must not be set as private. This bitbucket link can be added to a trainee either during the `Adding new Trainees` or `Editing Existing Trainees` steps. To update this information so jenkins knows which links to download you will need to push the changes to git on the master branch using the commands:
```
git add .
git commit -m 'updating trainee information'
git push
```


Test results are collected on jenkins when the trainee has decided to publish their work through a bashscript. You will be able to know when test results have finished being collected through the slack channel `jask-jenkins`.

You also have the option of triggering the test collection process by yourself by logging into jenkins at `http://jask-jenkins.academy.labs.automationlogic.com:8080/` (login details will be sent to you securely) and triggering the build for job `trainee-build-python`

after either of the two methods the trainer will need to pull from master of this repo to get the latest test results data using the command:
```
git pull
```